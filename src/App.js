import "./App.css";
import { useEffect, useState } from "react";
import Header from "./components/header";
import Navigation from "./components/navigation";
import Homepage from "./containers/homepage";
import API from "./api/axios";
import Footer from "./components/footer";

const App = () => {
  const [user, setUser] = useState();

  const getUsers = () => {
    API.get(`users`).then(({ data }) => {
      setUser(data.data[0]);
    });
  };

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <>
      <Header user={user} />
      <Navigation />
      <Homepage />
      <Footer />
    </>
  );
};

export default App;
