import React, { useEffect, useState } from "react";
import Reports from "../../components/reports";
import API from "../../api/axios";

const Homepage = () => {
  const [report, setReport] = useState();
  const [endDate, setEndDate] = useState();
  const [projects, setProjects] = useState([]);
  const [gateways, setGateways] = useState([]);
  const [startDate, setStartDate] = useState();
  const [projectLabel, setProjectLabel] = useState();
  const [selectedProject, setSelectedProject] = useState(0);
  const [selectedGateway, setSelectedGateway] = useState(0);
  const [selectedProjectTitle, setSelectedTitleProject] = useState();
  const [selectedGatewayTitle, setSelectedTitleGateway] = useState();

  const getProjects = () => {
    API.get(`projects`).then(({ data }) => {
      setProjects(data.data);
    });
  };

  const getGateways = () => {
    API.get(`gateways`).then(({ data }) => {
      setGateways(data.data);
    });
  };

  const resetReport = () => {
    projects.map((project) => {
      delete project.payments;
      delete project.total;
    });
    gateways.map((gateway) => {
      delete gateway.payments;
      delete gateway.total;
    });
  };

  const getReport = () => {
    resetReport();
    const body = {
      from: startDate,
      to: endDate,
      projectId: selectedProject == 0 ? undefined : selectedProject,
      gatewayId: selectedGateway == 0 ? undefined : selectedGateway,
    };

    setSelectedTitleProject(selectedProject);
    setSelectedTitleGateway(selectedGateway);

    API.post(`report`, body).then(({ data }) => {
      if (selectedProject != 0 && selectedGateway == 0) {
        data.data.map((payment) => sortPaymentForGateway(payment));
        setProjectLabel(false);
      } else {
        data.data.map((payment) => sortPaymentForProject(payment));
        setProjectLabel(true);
      }
      setReport(data.data);
    });
  };

  const calculateTotal = (array) => {
    let total = 0;
    array.map((item) => {
      total += item.total ? item.total : 0;
    });
    return total;
  };

  const getGatewayName = (id) => {
    return gateways.find((gateway) => gateway.gatewayId == id);
  };

  const getProjectName = (id) => {
    return projects.find((project) => project.projectId == id);
  };

  const sortPaymentForGateway = (payment) => {
    const tempGateway = gateways.find(
      (gateway) => gateway.gatewayId === payment.gatewayId
    );

    tempGateway.total = tempGateway.total
      ? tempGateway.total + payment.amount
      : payment.amount;
    tempGateway.payments = tempGateway.payments
      ? [...tempGateway.payments, payment]
      : [payment];
  };

  const sortPaymentForProject = (payment) => {
    const tempProject = projects.find(
      (project) => project.projectId === payment.projectId
    );

    tempProject.total = tempProject.total
      ? tempProject.total + payment.amount
      : payment.amount;
    tempProject.payments = tempProject.payments
      ? [...tempProject.payments, payment]
      : [payment];
  };

  useEffect(() => {
    getProjects();
    getGateways();
  }, []);

  useEffect(() => {
    getProjects();
    getGateways();
  }, []);

  return (
    <Reports
      report={report}
      endDate={endDate}
      projects={projects}
      gateways={gateways}
      getReport={getReport}
      startDate={startDate}
      setEndDate={setEndDate}
      projectLabel={projectLabel}
      setStartDate={setStartDate}
      calculateTotal={calculateTotal}
      getGatewayName={getGatewayName}
      getProjectName={getProjectName}
      selectedProject={selectedProject}
      selectedGateway={selectedGateway}
      setSelectedProject={setSelectedProject}
      setSelectedGateway={setSelectedGateway}
      selectedGatewayTitle={selectedGatewayTitle}
      selectedProjectTitle={selectedProjectTitle}
    />
  );
};

export default Homepage;
