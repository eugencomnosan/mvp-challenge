import React from "react";
import styled from "styled-components";

const Header = (props) => {
  const { user } = props;
  return (
    <HeaderWrapper>
      <BlockWrapper>
        <LogoWrapper src={"/logo.svg"} alt="logo" />
        <MenuWrapper src={"/hamburgerMenu.svg"} alt="logo" />
      </BlockWrapper>
      <BlockWrapper>
        <InitialsWrapper>
          {user?.firstName[0] + user?.lastName[0]}
        </InitialsWrapper>
        <NameWrapper>{user?.firstName + " " + user?.lastName}</NameWrapper>
      </BlockWrapper>
    </HeaderWrapper>
  );
};
export default Header;

const HeaderWrapper = styled.div`
  height: 80px;
  display: flex;
  justify-content: space-between;
  border-bottom: 2px solid #e5e5e5;
`;

const BlockWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  padding: 20px 0px 20px 35px;
`;

const LogoWrapper = styled.img`
  cursor: pointer;
  margin-right: 38px;
`;

const MenuWrapper = styled.img`
  height: 30px;
  cursor: pointer;
`;

const InitialsWrapper = styled.div`
  width: 43px;
  color: white;
  height: 43px;
  display: flex;
  cursor: pointer;
  font-size: 23px;
  font-weight: 700;
  border-radius: 5px;
  align-items: center;
  justify-content: center;
  background-color: #f6ca65;
}
`;

const NameWrapper = styled.div`
  color: #005b96;
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  margin-left: 10px;
  margin-right: 100px;
`;
