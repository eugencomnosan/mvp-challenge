import React from "react";
import styled from "styled-components";

const NoReports = () => {
  return (
    <NoReportsWrapper>
      <HeaderStyled>No reports</HeaderStyled>
      <DescriptionStyled>
        {"Currently you have no data for the reports to be generated."}
        <br />
        {"Once you start generating traffic trough the Balance application"}
        <br /> {"the reports will be shown."}
      </DescriptionStyled>
      <img src={"/noReports.svg"} alt="logo" />
    </NoReportsWrapper>
  );
};

export default NoReports;

const NoReportsWrapper = styled.div`
  display: flex;
  margin-top: 140px;
  align-items: center;
  justify-items: center;
  flex-direction: column;
`;

const HeaderStyled = styled.div`
  color: #011f4b;
  font-size: 24px;
  font-weight: 700;
  margin-bottom: 5px;
`;

const DescriptionStyled = styled.div`
  color: #7e8299;
  font-size: 16px;
  max-width: 500px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 50px;
  line-height: 18.75px;
`;
