import React from "react";
import ReactDatePicker from "react-datepicker";
import styled from "styled-components";
import NoReports from "./NoReports";
import ReportTable from "./ReportTable";
import "react-datepicker/dist/react-datepicker-cssmodules.css";

const Reports = (props) => {
  const {
    report,
    endDate,
    projects,
    gateways,
    startDate,
    getReport,
    setEndDate,
    projectLabel,
    setStartDate,
    getGatewayName,
    calculateTotal,
    getProjectName,
    selectedProject,
    selectedGateway,
    setSelectedGateway,
    setSelectedProject,
    selectedProjectTitle,
    selectedGatewayTitle,
  } = props;

  return (
    <HomepageWrapper>
      <FilterRow>
        <ReportsText>{"Reports"}</ReportsText>
        <FilterWrapper>
          <StyledSelect
            value={selectedProject}
            onChange={(e) => setSelectedProject(e.target.value)}
          >
            <option defaultValue value={"0"}>
              {"All projects"}
            </option>
            {projects.map((project) => (
              <option key={project.projectId} value={project.projectId}>
                {project.name}
              </option>
            ))}
          </StyledSelect>

          <StyledSelect
            value={selectedGateway}
            onChange={(e) => setSelectedGateway(e.target.value)}
          >
            <option defaultValue value={"0"}>
              {"All gateways"}
            </option>
            {gateways.map((gateway) => (
              <option value={gateway.gatewayId}>{gateway.name}</option>
            ))}
          </StyledSelect>
          <DatePickerStyled
            selected={startDate}
            placeholderText="From date"
            onChange={(date) => setStartDate(date)}
          ></DatePickerStyled>

          <DatePickerStyled
            selected={endDate}
            placeholderText="To date"
            onChange={(date) => setEndDate(date)}
          ></DatePickerStyled>

          <StyledButton onClick={() => getReport()}>
            {"Generate Report"}
          </StyledButton>
        </FilterWrapper>
      </FilterRow>
      <DescriptionRow>
        {"Easily generate a report of your transactions"}
      </DescriptionRow>

      {report && report.length > 0 ? (
        <ReportTable
          report={report}
          projectLabel={projectLabel}
          totalAmount={calculateTotal}
          getProjectName={getProjectName}
          getGatewayName={getGatewayName}
          selectedProject={selectedProjectTitle}
          selectedGateway={selectedGatewayTitle}
          projects={projectLabel ? projects : gateways}
        />
      ) : (
        <NoReports />
      )}
    </HomepageWrapper>
  );
};

export default Reports;

const HomepageWrapper = styled.div`
  margin: 35px 100px;
  min-height: Calc(100vh - 190px);
`;

const FilterRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const ReportsText = styled.div`
  color: #011f4b;
  font-size: 24px;
  font-weight: 700;
`;

const DescriptionRow = styled.div`
  color: #7e8299;
  font-weight: 700;
`;

const StyledSelect = styled.select`
  border: none;
  height: 32px;
  color: white;
  font-size: 14px;
  margin-left: 23px;
  border-radius: 5px;
  padding-left: 12px;
  padding-right: 52px;
  background-color: #1bc5bd;

  -moz-appearance: none;
  -webkit-appearance: none;
  background-position-y: 50%;
  background-repeat: no-repeat;
  background-position-x: Calc(100% - 12px);
  background-image: url("/selectArrow.svg");
`;

const StyledButton = styled.button`
  color: white;
  width: 118px;
  height: 32px;
  border: none;
  font-size: 14px;
  margin-left: 23px;
  border-radius: 5px;
  background-color: #005b96;
`;

const DatePickerStyled = styled(ReactDatePicker)`
  width: 98px;
  color: white;
  height: 32px;
  border: none;
  font-size: 14px;
  margin-left: 23px;
  border-radius: 5px;
  padding-left: 20px;
  background-color: #1bc5bd;

  -moz-appearance: none;
  -webkit-appearance: none;
  background-position-y: 50%;
  background-repeat: no-repeat;
  background-image: url("/calendar.svg");
  background-position-x: Calc(100% - 12px);
`;

const FilterWrapper = styled.div`
  display: flex;
`;
