import React from "react";
import styled from "styled-components";
import Chart from "./pieChart";

const ReportTable = (props) => {
  const {
    projects,
    totalAmount,
    getGatewayName,
    selectedProject,
    selectedGateway,
    getProjectName,
  } = props;

  const formatDate = (date) => {
    const newDate = new Date(date);
    let year = newDate.getFullYear();

    let month = (1 + newDate.getMonth()).toString();
    month = month.length > 1 ? month : "0" + month;

    let day = newDate.getDate().toString();
    day = day.length > 1 ? day : "0" + day;

    return month + "/" + day + "/" + year;
  };

  let showGateway = selectedGateway == 0 && selectedProject == 0;

  let showTitle = selectedGateway != 0 && selectedProject != 0;

  let hideChart = showGateway || showTitle;

  return (
    <ReportWrapper hideChart={hideChart}>
      <TableWrapper hideChart={hideChart}>
        <TitleStyled>
          <div>
            {selectedProject != 0
              ? getProjectName(selectedProject).name
              : "All projects"}{" "}
            |{" "}
            {selectedGateway != 0
              ? getGatewayName(selectedGateway).name
              : "All gateways"}
          </div>
        </TitleStyled>
        {projects.map((project) => {
          let iterator = 0;

          return (
            project.payments && (
              <>
                <>
                  {!showTitle && (
                    <ProjectTotalStyled>
                      <div>{project.name} </div>
                      <div>TOTAL {project?.total?.toFixed(2)} USD</div>
                    </ProjectTotalStyled>
                  )}
                  <TableHeaderStyled>
                    <RowWidth>Date</RowWidth>
                    {showGateway && (
                      <RowWidth align={"center"}>Gateway</RowWidth>
                    )}
                    <RowWidth align={"center"}>Transaction ID</RowWidth>
                    <RowWidth align={"end"}>Amount</RowWidth>
                  </TableHeaderStyled>
                  {project.payments?.map((payment, index) => {
                    iterator += 1;

                    return (
                      <TableRowStyled
                        key={index}
                        color={iterator % 2 == 0 ? "white" : ""}
                      >
                        <RowWidth>{formatDate(payment.created)}</RowWidth>
                        {showGateway && (
                          <RowWidth align={"center"}>
                            {getGatewayName(payment.gatewayId).name}
                          </RowWidth>
                        )}
                        <RowWidth align={"center"}>
                          {payment.paymentId}{" "}
                        </RowWidth>
                        <RowWidth align={"end"}>{payment.amount} USD</RowWidth>
                      </TableRowStyled>
                    );
                  })}
                </>
              </>
            )
          );
        })}
      </TableWrapper>
      {!hideChart && (
        <Chart unrefinedData={projects} totalAmount={totalAmount} />
      )}
      <>
        {showTitle && (
          <TotalStyled>
            <div>TOTAL | {totalAmount(projects).toFixed(2)} USD</div>
          </TotalStyled>
        )}
      </>
    </ReportWrapper>
  );
};

export default ReportTable;

const TitleStyled = styled.div`
  display: flex;
  color: #011f4b;
  font-size: 16px;
  font-weight: 700;
  margin-bottom: 35px;
  justify-content: space-between;
`;

const TableWrapper = styled.div`
  padding: 20px;
  margin-top: 37px;
  border-radius: 10px;
  background-color: #f1fafe;
  margin-right: ${({ hideChart }) => (!hideChart ? `30px` : ``)};
  width: ${({ hideChart }) => (!hideChart ? `Calc(100% - 560px)` : ``)};
`;

const ProjectTotalStyled = styled.div`
  display: flex;
  padding: 25px;
  color: #011f4b;
  font-size: 16px;
  font-weight: 700;
  margin-bottom: 5px;
  border-radius: 10px;
  background-color: white;
  justify-content: space-between;
`;

const TotalStyled = styled.div`
  display: flex;
  padding: 25px;
  color: #011f4b;
  font-size: 16px;
  margin-top: 25px;
  font-weight: 700;
  margin-bottom: 5px;
  border-radius: 10px;
  background-color: white;
  background-color: #f1fafe;
  justify-content: space-between;
`;

const TableHeaderStyled = styled.div`
  height: 35px;
  display: flex;
  color: #011f4b;
  font-size: 16px;
  margin-botton: 3px;
  align-items: center;
  background-color: white;
  margin: 7px 0px 7px 35px;
  padding: 5px 25px 5px 5px;
  justify-content: space-between;
`;

const TableRowStyled = styled.div`
  display: flex;
  margin: 7px 0px 7px 35px;
  padding: 5px 25px 5px 5px;
  justify-content: space-between;
  background-color: ${({ color }) => color};
`;

const RowWidth = styled.div`
  width: 25%;
  color: #011f4b;
  text-align: ${({ align }) => align};
`;

const ReportWrapper = styled.div`
  display: flex;
  flex-direction: ${({ hideChart }) => (hideChart ? "column" : "row")};
`;
