import React, { useState, useEffect } from "react";
import { PieChart } from "react-minimal-pie-chart";
import styled from "styled-components";

// this would probably come from BE, or we can generate a random array depending of the needs
// this one is just for mock
const colorPallet = [
  "#DAF7A6",
  "#FFC300",
  "#FF5733",
  "#C70039",
  "#900C3F",
  "#581845",
  "33FFB5",
  "#3349FF",
  "#FF33BB",
  "#FF3333",
];

const Chart = (props) => {
  const { unrefinedData, totalAmount } = props;

  let data = unrefinedData.map((item, index) => {
    return {
      title: item.name,
      value: item.total,
      color: colorPallet[index],
    };
  });

  const defaultLabelStyle = {
    fontSize: "10px",
    fontFamily: "sans-serif",
    fill: "#fff",
  };
  const lineWidth = 60;

  return (
    <Chartwrapper>
      <NavStyled>
        {data?.map((item) => {
          return (
            <TitleWrapper color={item.color}>
              <ColloredBox color={item.color} /> {item.title}
            </TitleWrapper>
          );
        })}
      </NavStyled>
      <PieChart
        data={data}
        lineWidth={60}
        labelStyle={defaultLabelStyle}
        labelPosition={100 - lineWidth / 2}
        radius={PieChart.defaultProps.radius}
        style={{ height: "270px", width: "270px" }}
        label={({ dataEntry }) => Math.round(dataEntry.percentage) + "%"}
      />
      <TotalStyled>
        TOTAL | {totalAmount(unrefinedData).toFixed(2)} USD
      </TotalStyled>
    </Chartwrapper>
  );
};
export default Chart;

const NavStyled = styled.div`
  width: 490px;
  padding: 20px;
  display: flex;
  border-radius: 10px;
  margin-bottom: 90px;
  background-color: #f1fafe;
`;

const Chartwrapper = styled.div`
  width: 520px;
  display: flex;
  margin-top: 37px;
  align-items: center;
  flex-direction: column;
`;

const ColloredBox = styled.div`
  width: 15px;
  height: 15px;
  border-radius: 5px;
  margin-right: 10px;
  background-color: ${({ color }) => color};
`;

const TitleWrapper = styled.div`
  display: flex;
  margin-right: 40px;
`;

const TotalStyled = styled.div`
  width: 490px;
  display: flex;
  padding: 20px;
  color: #011f4b;
  font-size: 16px;
  font-weight: 700;
  margin-top: 90px;
  margin-bottom: 5px;
  border-radius: 10px;
  background-color: white;
  background-color: #f1fafe;
`;
