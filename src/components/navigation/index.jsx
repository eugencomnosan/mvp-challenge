import React from "react";
import styled from "styled-components";

const Navigation = () => {
  return (
    <NavWrapper>
      <MenuItem src={"/stats.svg"} alt="logo" />
      <MenuItem src={"/menu.svg"} alt="logo" />
      <MenuItem src={"/payments.svg"} alt="logo" />
      <MenuItem src={"/chart.svg"} alt="logo" />
      <MenuItem src={"/logout.svg"} alt="logo" />
    </NavWrapper>
  );
};

export default Navigation;

const NavWrapper = styled.div`
  width: 100px;
  display: flex;
  margin-top: 35px;
  position: absolute;
  align-items: center;
  flex-direction: column;
`;

const MenuItem = styled.img`
  width: 24px;
  height: 24px;
  cursor: pointer;
  margin-bottom: 24px;
`;
