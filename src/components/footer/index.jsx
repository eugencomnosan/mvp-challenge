import React from "react";
import styled from "styled-components";

//this would have been 2 links towards TM & PP
// but for this use case its only design fit
const Footer = () => {
  return <FooterStyled>{"Terms&Conditions | Privacy policy"}</FooterStyled>;
};

export default Footer;

const FooterStyled = styled.div`
  color: #005b96;
  cursor: pointer;
  font-size: 16px;
  font-weight: 700;
  margin-left: 100px;
  margin-bottom: 20px;
`;
